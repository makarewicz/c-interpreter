module Evaluator where

import Absc
import ErrM
import Env
import Control.Monad.Error

type Result = IOThrowsError ()

failure :: Show a => a -> Result
failure x = throwError $ Default $ "Undefined case: " ++ show x

transProgram :: Env -> Program -> Result
transProgram env x = case x of
  PDefs stms -> mapM_ (transStm env) stms 

transDef :: Env -> Def -> Result
transDef env x = case x of
  DFun type' id args stm  -> do
      fun <- return $ Fun (map transArg args) env stm type'
      defineVar env id fun
      return ()

transArg :: Arg -> (Type, Id)
transArg x = case x of
  ADecl type' id  -> (type', id)

transDecl :: Env -> Decl -> Result
transDecl env x = case x of
  DDecls type' id  -> do
      defineVar env id (getDefaultInstance type')
      return ()
  DInit type' id exp  -> do
      val <- transExp env exp
      castedVal <- castToType type' val
      defineVar env id castedVal
      return ()

transStm :: Env -> Stm -> IOThrowsError ()
transStm env x = case x of
  SFunction def  -> transDef env def
  SExp exp  -> do _ <- transExp env exp
                  return ()
  SDecls decl  -> transDecl env decl
  SReturn exp  -> do
      val <- transExp env exp
      throwError $ ReturnVar val
  SReturnVoid  -> throwError $ ReturnVar Void
  SFor decl exp1 exp2 stm  ->
    transStm env (SBlock [SDecls decl, SWhile exp1 (SBlock [stm, SExp exp2])])
  SWhile exp stm  -> do
      val <- transExp env exp
      if isTrue val then do
          transStm env stm
          transStm env (SWhile exp stm)
      else
          return ()
  SBlock stms  -> do envCopy <- liftIO $ copyEnv env
                     mapM_ (transStm envCopy) stms
  SIfElse exp stm1 stm2  -> do val <- transExp env exp
                               if isTrue val then transStm env stm1
                                             else transStm env stm2
  SPrint exp  -> do val <- transExp env exp
                    liftIO $ putStrLn $ show val

numericBinOp :: Env -> Exp -> Exp -> (Double -> Double -> Double) -> IOThrowsError (CVal) 
numericBinOp env exp1 exp2 op = do
        val1 <- transExp env exp1
        val2 <- transExp env exp2
        v1 <- cValToDouble val1
        v2 <- cValToDouble val2
        return $ Double $ op v1 v2

comparisonBinOp :: Env -> Exp -> Exp -> (Double -> Double -> Bool) -> IOThrowsError (CVal) 
comparisonBinOp env exp1 exp2 op = do
        val1 <- transExp env exp1
        val2 <- transExp env exp2
        v1 <- cValToDouble val1
        v2 <- cValToDouble val2
        return $ Bool $ op v1 v2

logicalBinOp :: Env -> Exp -> Exp -> (Bool -> Bool -> Bool) -> IOThrowsError (CVal) 
logicalBinOp env exp1 exp2 op = do
        (Bool val1) <- transExp env exp1
        (Bool val2) <- transExp env exp2
        return $ Bool $ op (val1) (val2)


transExp :: Env -> Exp -> IOThrowsError (CVal) 
transExp env x = case x of
  ETrue  -> return (Bool True)
  EFalse  -> return (Bool False)
  EInt n  -> return (Int n)
  EDouble d  -> return (Double d)
  EId id  -> do val <- getVar env id
                return val
  ECall id exps  -> do (Fun params closure body returnType) <- getVar env id
                       vals <- mapM (transExp env) exps
                       funEnv <- bindVars closure $ zip params vals
                       val <- catchError (transStm funEnv body >> return Void)
                                (\x -> case x of
                                    ReturnVar val -> return val
                                    _ -> throwError x)
                       castToType returnType val >>= return
  EPIncr id  -> do val <- getVar env id
                   v <- cValToDouble val
                   newVal <- return $ Double (v + 1.0)
                   setVar env id newVal
                   return val
  EPDecr id  -> do val <- getVar env id
                   v <- cValToDouble val
                   newVal <- return $ Double (v - 1.0)
                   setVar env id newVal
                   return val
  EIncr id  -> do val <- getVar env id
                  v <- cValToDouble val
                  newVal <- return $ Double (v + 1.0)
                  setVar env id newVal
                  return newVal
  EDecr id  -> do val <- getVar env id
                  v <- cValToDouble val
                  newVal <- return $ Double (v - 1.0)
                  setVar env id newVal
                  return newVal
  ETimes exp1 exp2  -> numericBinOp env exp1 exp2 (*)
  EDiv exp1 exp2  -> numericBinOp env exp1 exp2 (/)
  EPlus exp1 exp2  -> numericBinOp env exp1 exp2 (+)
  EMinus exp1 exp2  -> numericBinOp env exp1 exp2 (-)
  ELt exp1 exp2  -> comparisonBinOp env exp1 exp2 (<)
  EGt exp1 exp2  -> comparisonBinOp env exp1 exp2 (>)
  ELtEq exp1 exp2  -> comparisonBinOp env exp1 exp2 (<=)
  EGtEq exp1 exp2  -> comparisonBinOp env exp1 exp2 (>=)
  EEq exp1 exp2  -> comparisonBinOp env exp1 exp2 (==)
  ENEq exp1 exp2  -> comparisonBinOp env exp1 exp2 (/=)
  EAnd exp1 exp2  -> logicalBinOp env exp1 exp2 (&&)
  EOr exp1 exp2  -> logicalBinOp env exp1 exp2 (||)
  EAss id exp  -> do val <- transExp env exp
                     setVar env id val
                     return val
  ETAss id exp  -> transExp env (EAss id (ETimes (EId id) exp))
  EDAss id exp  -> transExp env (EAss id (EDiv (EId id) exp))
  EPAss id exp  -> transExp env (EAss id (EPlus (EId id) exp))
  EMAss id exp  -> transExp env (EAss id (EMinus (EId id) exp))
  ETyped exp type'  -> do val <- transExp env exp
                          typed <- castToType type' val
                          return typed

