prog:
	ghc --make Interpreter.hs -o Interpreter

bnfc_files:
	happy -gca Parc.y
	alex -g Lexc.x
	latex Docc.tex; dvips Docc.dvi -o Docc.ps
	ghc --make Testc.hs -o Testc
clean:
	-rm -f *.log *.aux *.hi *.o *.dvi
	-rm -f Docc.ps
distclean: clean
	-rm -f Docc.* Lexc.* Parc.* Layoutc.* Skelc.* Printc.* Testc.* Absc.* Testc ErrM.* SharedString.* c.dtd XMLc.* Makefile*

