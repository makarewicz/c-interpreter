module Env where

import Data.IORef
import Control.Monad.Error
import Absc
import qualified Data.Map as Map

data CError = UnboundVar String Id
            | Default String
            | ReturnVar CVal -- pseudo error used to return values from functions.
            | WrongType String

instance Error CError where
        noMsg = Default "An error has occured"
        strMsg = Default

showError :: CError -> String
showError (UnboundVar message (Id varname)) = message ++ ": " ++ varname
showError (Default message) = message
showError (ReturnVar val) = "Return out of scope."
showError (WrongType message) = message

instance Show CError where show = showError

type ThrowsError = Either CError

trapError action = catchError action (return . show)

extractValue (Right val) = val

type IOThrowsError = ErrorT CError IO

liftThrows :: ThrowsError a -> IOThrowsError a
liftThrows (Left err) = throwError err
liftThrows (Right val) = return val

data CVal = Bool Bool
          | Int Integer
          | Double Double
          | Fun { params :: [(Type, Id)], closure :: Env, body :: Stm, returnType :: Type}
          | Void

getDefaultInstance type' = case type' of
    Type_bool -> Bool False
    Type_int -> Int 0
    Type_double -> Double 0.0
    Type_void -> Void

instance Show CVal where
    show x = case x of
        Bool v -> show v
        Int v -> show v
        Double v -> show v

isTrue x = case x of
    Bool b -> b
    Int v -> v == 0
    Double v -> v == 0
    Fun v e s t -> True
    Void -> False

castToType :: Type -> CVal ->  IOThrowsError(CVal)
castToType typ val = case typ of
    Type_bool -> return $ Bool (isTrue val)
    Type_int -> do
        v <- cValToInt val
        typed <- return $ Int v
        return typed
    Type_double -> do
        v <- cValToDouble val
        typed <- return $ Double v
        return typed
    _ -> throwError $ WrongType "Can't cast to type"

cValToDouble :: CVal -> IOThrowsError(Double)
cValToDouble x = case x of
    Int v -> return $ fromIntegral v
    Double v -> return $ v 
    _ -> throwError $ WrongType "Can't cast type to Double"

cValToInt :: CVal -> IOThrowsError (Integer)
cValToInt x = case x of
    Int v -> return $ v
    Double v -> return $ floor v
    _ -> throwError $ WrongType "Can't cast type to Int"

type Env = IORef (Map.Map Id (IORef CVal))

emptyEnv :: IO Env
emptyEnv = newIORef Map.empty

isBound :: Env -> Id -> IO Bool
isBound envRef var = readIORef envRef >>= return . maybe False (const True) . Map.lookup var

getVar :: Env -> Id -> IOThrowsError CVal
getVar envRef var  =  do env <- liftIO $ readIORef envRef
                         maybe (throwError $ UnboundVar "Getting an unbound variable" var)
                               (liftIO . readIORef)
                               (Map.lookup var env)

setVar :: Env -> Id -> CVal -> IOThrowsError CVal
setVar envRef var value = do env <- liftIO $ readIORef envRef
                             maybe (throwError $ UnboundVar "Setting an unbound variable" var) 
                                   (liftIO . (flip writeIORef value))
                                   (Map.lookup var env)
                             return value

defineVar :: Env -> Id -> CVal -> IOThrowsError CVal
defineVar envRef var value = do 
    alreadyDefined <- liftIO $ isBound envRef var 
    if alreadyDefined 
        then setVar envRef var value >> return value
        else liftIO $ do 
             valueRef <- newIORef value
             env <- readIORef envRef
             writeIORef envRef $ Map.insert var valueRef env
             return value

copyEnv :: Env -> IO Env
copyEnv envRef = readIORef envRef >>= newIORef >>= return

bindVars :: Env -> [((Type, Id), CVal)] -> IOThrowsError (Env)
bindVars envRef bindings = do
        env <- liftIO $ readIORef envRef
        extendedEnv <- extendEnv bindings env
        liftIO $ newIORef extendedEnv
        where extendEnv bindings env = foldM addBinding env bindings
              addBinding env ((type', var), value) = do typedVal <- castToType type' value
                                                        ref <- liftIO $ newIORef typedVal
                                                        return $ Map.insert var ref env

